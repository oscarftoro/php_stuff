

<footer id = "pageFooter">

    <div id="licence_box">
    <span id ="creative_commons">
    <a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/deed.da">
        <img alt="Creative Commons licens" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/3.0/88x31.png" /></a>
        <span class="text">Dette værk er licenseret under en <a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/deed.da">Creative Commons Navngivelse-IkkeKommerciel 3.0 Unported Licens.<br></a></span>
        <span class="text">by <span id= "oscar">Oscar Toro </span>to Web Development Course DAT12W (Computer Science) at Københavns Ehrvevsakademi</span>
    </span>
    </div>
    <div class = "social">
        <ul>
            <li> <a href= "http://twiter.com/of_toro" class = "twitter"></a></li>
            <li> <a href= "http://facebook.com/OscFT" class = "facebook"></a></li>
            <li><a href="http://github.com/oscarfelipe" class="github"></a></li>
            <li> <a href= "index.php" class = "mail"></a></li>
        </ul>
    </div>
</footer>
