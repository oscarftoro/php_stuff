<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href= "style.css" media="screen">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<html>
<head>
    <title> PHP Web Development </title>
</head>
<body>

<?php
$base_path = $_SERVER["DOCUMENT_ROOT"];
include ("navigation.php"); ?>

<div id = "main_picture">
    <div class ="picture">

        <div id="headerTitle">PHP experiments <br/></div>
        <div id="headerSubtext">by Oscar Felipe Toro</div>
        <p> "Even if our efforts of attention seem for years to be producing no result,
            one day a light that is in exact proportion to them will flood the room." -Simon Weil </p>

    </div>
</div>

<div class = "note">
    <h1> Experiments 1</h1>
    <p> In this first round of experiments, I try out the potential of PHP as a scripting language to develop web applications.</p>
    <p> the first round of exercise focuses on the basics of PHP</p>
    <p>guest book
    image gallery
    reversing string
    palindrome test </p>

</div>
<div class = "note2">
    <h1> Experiments 2</h1>
    <p> A fair number of the programs we use every day are doing more than one thing at a time.
        Every web server, smartphone app, GUI application, operating system, chat server, VoIP system, game server,
        music player, video player, etc. that you’re using are concurrent programs -
        either in the sense that they are serving multiple users at once, or in the sense that they have
        multiple asynchronous event sources they have to react to in a timely manner.</p>
    <p> Yet most programming languages - and about all mainstream languages -
        aren’t very well geared towards making concurrent applications.
        The programming paradigms one hears most about these days are the Object-oriented and the Functional paradigm.
        Ever heard of Concurrency-oriented languages? They exist, but there aren’t very many of them -
        and they are not mainstream but something you have to actively seek out.</p>
</div>

<?php include "footer.php"; ?>

</body>
</html>
