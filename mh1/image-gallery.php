<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 9/14/13
 * Time: 12:28 PM
 * mh1_2
 * Make an image gallery. It should load all image files from a given folder, and present these nicely on a web page.
 * Challenge: Pagination with links, so the user can select image groups: 1-10, 11-20, 21-30 and so on.
 */
$dir ='images';
$dirh = opendir($dir);
$files = array();
$counter = 0;
while(false !== ($filename = readdir($dirh))) {
    $files[] = $filename;
    $counter+=  1;
}

?>

<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href= "../style.css" media="screen">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<html>
<head>
    <title> Image gallery - Experiments 1 </title>
</head>
<body>
<? include "../navigation.php" ?>
<h1>Image Gallery</h1>
<div id= "center-the-stuff">
<?php
if (isset($files)){

    foreach($files as $image) {

        echo "<img src=\"images/$image\"/>";
    }

}
$pages = $counter /10;
$last_page =$counter % 10;
$limit = 10;
echo "</div >";
?>

<?php include "../footer.php";

?>

</body>
</html>
