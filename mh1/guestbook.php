<?php

/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 9/3/13
 * Time: 12:29 PM
 * Make a file-based guestbook.
 * The user should enter a name and a message.
 * The date and time of each message should automatically be saved.
 * mh1_1
 */

//session_start(); //needed to maintain the state
$filename= "messages.txt";
$file_handle = fopen($filename,"a+");

fclose($file_handle);
/*
 * Add a message to a file called message.txt
 */
function add($user, $message){
    $now = strftime("%d/%B/%G " . "at %H:%M"); //time
    global $filename;
    $file_handle = fopen($filename,"a");
    //acid transaction to write the file...
    if(flock($file_handle,LOCK_EX)) {  //take the lock and try to write...
            $trimmed=rtrim($message,"\n");
            if(fwrite($file_handle,("<h2> $user</h2>\n" . "<h4>$now</h4> \n" . "<p>$message</p>". "\n")) == FALSE) {
                echo "Cannot write to file($filename)";

            }
            flock($file_handle, LOCK_UN); //release the lock
    }
        fclose($file_handle);

}



function read_file($filename){
    $handle =fopen($filename, 'r') or exit("Unable to open file!");
    //$counter =1;
    while(!feof($handle)){
       echo fgets($handle);
    }
    fclose($handle);

}

?>
<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href= "../style.css">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<html>
<head>
    <title> PHP Web Development - Experiments 1 </title>
</head>
<body>
<? include "../navigation.php" ?>

<h1> Guest book</h1>
<form name="guestBook" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">

        <label for = "name" >Name:</label>
        <input type="text" name="name" id="name">


     <label for ="message">Message: </label>
        <textarea name="message" id="message" ></textarea><br>



    <input type="submit" name ="send" id="send" value="Send Message">

</form>
<div>
    <h1>Messages...</h1>

    <?php
    //if the file is there print their content
    //otherwise take _POST and print it as a message
    if(filesize($filename) > 0) {
        read_file($filename);
    }
    if(isset($_POST['name']) && isset($_POST['message'])) {
        $now = strftime("%d/%B/%G " . "at %H:%M"); //time
        add($_POST['name'],$_POST['message']);
        echo "<h2><b></b> {$_POST['name']} </b></h2>
              <h4> $now</h4>
              <p> {$_POST['message']}</p>
        ";
    }

    ?>

</div>


<?php include "../footer.php"; ?>


</body>
</html>



