<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/28/13
 * Time: 11:22 PM
 * To change this template use File | Settings | File Templates.
 */

?>
<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href= "../style.css">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<html>
<head>
    <title> PHP Web Development - Experiments 1 </title>
</head>
<body>
<? include "../navigation.php" ?>

<div class ="experiments">
    <h1>Experiments 1</h1>
<ul>

    <li><a href="/mh1/guestbook.php">guest-book</a></li>
    <li><a href="/mh1/image-gallery.php">image gallery</li>
    <li><a href="/mh1/string-reverser.php">strings</li>

</ul>
</div>

<?php include "../footer.php";

?>



</body>
</html>

