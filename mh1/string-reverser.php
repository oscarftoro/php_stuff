<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 9/14/13
 * Time: 5:49 PM
 * Make a form, that asks the user for a string of text to reverse. When submitted, the server should return the string in reverse.
 * For example, if the user enters the words "Hello World!" the output to the user should be "!dlroW olleH".
 */
$reversed = array();
function reverse($to_reverse){

    // for($i =0; $i < )
    /*rev($to_reverse,strlen($to_reverse));

    $result = array_reverse((array)$to_reverse);
    foreach ( $result as $letter) {
        echo "<p> " . $letter . " </p>";

    }*/
}

function rev($string,$int){
    global $reversed;

    if($int == 0){
        echo "";
    } else{
        rev($string,$int-1 );
    }
}

function lazyRev($string){
    return strrev($string);
}

function check_palindrome($string){
    $trimmed = str_replace(" ", "", $string);
    $unspaced = preg_replace('/\s+/', '', $string);

    if($trimmed == strrev($trimmed)) {
        echo "yes, this is a palindrome";
    } else {
        echo "sorry, this is not a palindrome ";

        echo $trimmed;
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>String Reverser - Oscar Toro - DAT12W</title>
    <link href="../style.css" rel="stylesheet" type="text/css">
</head>
<body>
<? include "../navigation.php" ?>


<h1>String Reverser</h1>

<p>String reverser is a great tool that allows you to reverse your favorite strings.
</p>
<p>Write a text, press submit
    and let us serve you the rest. Only <b>DKK 10</b> per word! </p>
<form name="reverser" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>">
    <p>
        <label for = "string"> Your String </label>
        <input type = "text" name="string" height="50">

    </p>
    <p>
        <input type = "submit" name = "reverse" value="Reverse">
    </p>
    <div>
        <?php
        if(isset($_POST['string'])){

            //reverse the string
            echo "<p> " . lazyRev($_POST['string']) . " </p>";//{$_POST['string']}
        }

        ?>
    </div>
</form>
<div id ="note">
<h1>Palindrome checker</h1>

<p>Palindrome checker is a state-of-art string tool that allows you to check whether a word or text is a Palindrome or not.
</p>
<p>Write a text, press submit
    and let us serve you the rest. Only <b>DKK 10.5</b> per checked text! </p>
<form name="reverser" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>">
    <p>
        <label for = "string2"> Your String </label>
        <input type = "text" name="palindrome" height="50">

    </p>
    <p>
        <input type = "submit" name = "pal" value="Check">
    </p>
    <div>
        <?php
        if(isset($_POST['palindrome'])){
            //check palindrome
            echo "<p> " . check_palindrome($_POST['palindrome']) . " </p>";   //
        }

        ?>
    </div>

</div>
<?php include "../footer.php" ?>
</body>
</html>