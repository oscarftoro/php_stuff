<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/29/13
 * Time: 12:09 AM
 *
 */


?>

<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href= "style.css">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<html>
<head>
    <title></title>
</head>
<body>
<?php include "navigation.php"?>

<h1> About</h1>
<div class ="about">
<p> Hi, my name is Oscar Felipe Toro, I'm  a computer science student at Copenhagen School of Design and Technology and this web page
    is a collection of exercises for an elective course called Web Development. This was a four semester course
 under the tutoring of <a href="http://joneikholm.dk/"> Jón Eikhólm</a>.  </p>
<p>The course was based on the study of PHP to develop web applications but there was also a continous integration of diverse technologies and tools
    used in  web development. The source code of all the exercise is
on bitbucket and is going to be available by december 2013 when the course will have finished</p>

<p> During the lectures we had the opportunity to design dynamic web pages with several technologies: PHP, CSS 3, Javascript,JQuery,
    JSON, XML,GIT, MySQL and Wordpress among others. If you want to contact me send me an e-mail at
    <span class="oscar"> groovenino[at]gmail.com</span></p>

</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<?php include "footer.php" ?>
</body>
</html>
