<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/20/13
 * Time: 12:59 PM
 * security class
 */

function secure_session_start() {

    $session_name = 'secure_session_id'; // Set a custom session name
    $secure = false; // Set to true if using https.
    $http_only = true; // This stops javascript being able to access the session id.

    ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies.
    $cookieParams = session_get_cookie_params(); // Gets current cookies params.
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $http_only);
    session_name($session_name); // Sets the session name to the one set above.
    session_start(); // Start the php session
    session_regenerate_id(); // regenerated the session, delete the old one.

}

