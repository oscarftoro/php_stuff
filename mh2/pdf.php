<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/30/13
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
require("./fpdf/fpdf.php");
/* header*/

class PDF1 extends FPDF {
    function Header() {
        $this->setFont("Arial", '', 32);
        $this->cell (190,15, "Our Company", 'LRTB',0,'C');
        $this->ln();
        $this->ln();

    }

}

function data($array){

}
$pdf = new PDF1();
$pdf->addPage();


//Linking variables to PDF
//items
$item1 = $_POST['item_1'];
$item2 = $_POST['item_2'];
$item1_number = ($_POST['amount_item1'] * 25) ;
$item2_number = ($_POST['amount_item2'] * 15) ;
$quantity1 = "X ". $_POST['amount_item1'];
$item1_price = $item1_number. " DKK";
$quantity2 ="X ". $_POST['amount_item2'];

$tp = ((int)$item1_number + (int)$item2_number);
$total_net = $tp . "DKK";

$moms =  ((int)$tp * 0.25) . "DKK";
$total_invoice = ceil(((int)$tp + (float)$moms)) . "DKK"; //round the value up

$item2_price = $item2_number . " DKK";
$name = $_POST['customer_name'];
$address =  $_POST['customer_address'];
$city = $_POST['customer_city'];

//Our personal data

$pdf->setFont("Arial", '', 12);
$pdf->cell(30,5,"Vladisvlad Vej 64 2. t.v.",'C');
$pdf->ln();
$pdf->cell(30,5,"CPH K Denmark",'C');

/*Date*/
$pdf->cell(160,5, "18 oktober 2013",0,1,'R');

/*number of Invoice*/
$pdf->setFont("Arial",'',10);
$pdf->cell(0 ,5 ,"Invoice Nr. 0000001",0 ,1 ,'R');
$pdf->Ln();//new line

/*customer data */
$pdf->setFont("Arial",'B',12);
$pdf->cell(190,5,"Invoice to: ",0,1,'R') ;
//customer data
$pdf->setFont("Arial",'I',12);
$pdf->cell(190,5,$name,0,1,'R');
$pdf->cell(190,5,$address,0,1,'R');
$pdf->cell(190,5,$city,0,1,'R');


$pdf->ln();
$pdf->ln();
$pdf->ln();
$pdf->ln();

$pdf->setFont("Arial", 'B', 12);
$pdf->cell(0,10,"INVOICE ",'B','L');
$pdf->ln();
$pdf->ln();

//change the font

$pdf->setFont("Arial", '', 12);
//item 1
$pdf->cell(20,5,$item1, 0,0 ,'L');
///move to the right
$pdf->Cell(60);

//quantity item1
$pdf->cell(60,5,$quantity1,0,0,'L');

///move to the right
$pdf->Cell(30);

//price item1

$pdf->cell(30,5,$item1_price,0,1,'L');

//quantity item2

//item 2
$pdf->cell(20,10,$item2,0,0,'L');

///move to the right
$pdf->Cell(60);

//quantity item2
$pdf->cell(60,10,$quantity2,0,0,'L');

///move to the right
$pdf->Cell(30);

//price item2
$pdf->cell(30,10,$item2_price,0,1,'L');

//black line
$pdf->cell(0,10,"",'B','L');

$pdf->ln(); //space

$pdf->cell(120);  //to the right
//amount
$pdf->Cell(0,10,"AMOUNT", 0,0,'L');
//price item2
$pdf->cell(-3,10,$total_net,0,1,'R');

//MOMS
$pdf->cell(120);// move to the right
$pdf->Cell(0,5,"MOMS (25%)", 0,0,'L');
//price item2
$pdf->cell(-3,5,$moms,0,1,'R');

//TOTAL
$pdf->setFont("Arial", 'B', 12);
$pdf->cell(120);// move to the right
$pdf->Cell(0,10,"TOTAL", 0,0,'L');
//price item2
$pdf->cell(-3,10,$total_invoice,0,1,'R');

$pdf->ln(60);


//Payment data
$pdf->cell(15,7,"BANK: ",0,0,'L');
$pdf->setFont("Arial", '', 12);
$pdf->cell(10,7,"Danske Bank",0,1,'L');

$pdf->setFont("Arial", 'B', 12);
$pdf->cell(23,7,"ACCOUNT: ",0,0,'L');
$pdf->setFont("Arial", '', 12);
$pdf->cell(0,7," 043-456-334-667-754",0,1,'L');

$pdf->setFont("Arial", 'B', 12);
$pdf->cell(23,7,"PAYMENT: ",0,0,'L');
$pdf->setFont("Arial", '', 12);
$pdf->cell(0,7,"Net 8 days. Interest accrued at 2.0% per month thereafter",0,1,'L');

$pdf ->output();