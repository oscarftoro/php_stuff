<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/21/13
 * Time: 6:29 PM
 * MH2_ an optimized version of the login
 */
require "security.php";

secure_session_start();
/**
 * check that the username is a valid email
 * and password is larger than 5 characters
 *
 * @param $mail email
 * @param $pass password
 * @return bool
 */
function check_credential($mail,$pass){

    if((filter_var($mail,FILTER_VALIDATE_EMAIL) == false) || strlen($pass) < 5){
        return false;
    } else { return true; }
}
$credential_valid = true;
if(isset($_POST['submit']) ){

    $username = $_POST['user'];
    $password = $_POST['password'];
    //if user or passwords are invalid set variable $credential_valid accordingly
    $credential_valid = check_credential($username,$password);
    //check the database
    $db = new mysqli("localhost","user", "password","db");
    $sql = " SELECT  * FROM users WHERE user= ? and password= ?";
    $statement = $db -> prepare($sql);

    $statement->bind_param("ss", $username, $password);

    $statement->execute();
    $statement->bind_result($id,$db_name,$db_password);

    //fetch values from database
    $statement->fetch();

    if(($username == $db_name) && ($password == $db_password)) {

        $_SESSION['user'] = $username;
        $_SESSION['opgave'] = "/mh2/logout2.php";//mark the session as opgave_2
        header("Location: " . "/mh2/enter.php");
        exit;

    } else {

        echo " User or password incorrect...";
        echo $db_password;
    }

}
?>
<html>
<link href="../style.css" rel="stylesheet" type="text/css">
<body>

<? include "../navigation.php" ?>
   <p> Enter a valid user name (e-mail) and password</p>
<div class ="login">
    <form name="login" method ="post" action ="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" >
        <label for ="text"> User Name
        <input type="text" name ="user">  </input><br/>
        </label>
        <label for ="password">  Password
            <input type="password" name ="password"> <br/>
        </label>
        <input type ="submit" name="submit" value="login">  <br/>
        <?php
        if (!$credential_valid){
            echo "email or username not valid";
        }
        ?>
    </form>
</div>


<?php include "../footer.php"; ?>
</body>
</html>