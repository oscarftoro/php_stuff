<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 9/25/13
 * Time: 11:29 AM
 *
 */


?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
    <title>Mandatory Assignment 2-2 - Oscar Toro - DAT12W</title>
    <link href="../style.css" rel="stylesheet" type="text/css">
</head>
<body>
<? include "../navigation.php" ?>

<h1>My Checkbox</h1>
<!--this solution takes the values directly from $_POST Array -->
 <p> where do you want access to?</p>
<div>
<form name = "checkbox" method = "post" action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>">
    <p>
        <input type="checkbox" name = "red_building">Red Building  <br/>
        <input type="checkbox" name = "tree_house">Tree House  <br/>
        <input type="checkbox" name = "fraternity_club">Fraternity Club<br/>
        <input type="checkbox" name = "utility_barack">Utility Barack <br/>
        <input type="checkbox" name = "oak_complex">Oak Complex<br/>
        <input type = "submit" name = "submit" value = "submited1">

    </p>
    </form>

    <?php
    if(isset($_POST['submit'])){
        $buildings = (sizeof($_POST) - 1);
        echo "<p>" . "You selected ".$buildings." door";
        // put an s on plural
        if($buildings > 1) echo "(s): "; else echo ": ";
        //check the buildings selected by the user
        foreach($_POST as $key => $value){
            if ($value == "on")
                echo $key . " ";
        }
        echo "<br/>";
        echo "Price is ".($buildings * 10) . " ";
        echo "size " . count($_POST) . "</p>";
        /*print_r($_POST);*/

    }
    ?>
</div>
<!--in this solution I use an array to take the values from the checkbox (my_form[])-->

<h1>My Checkbox Solution 2</h1>
<p> where do you want access to?</p>
<div>
    <form name = "checkbox" method = "post" action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>">
        <p>
            <input type="checkbox" name = "my_form[]" value="Red Building">Red Building  <br/>
            <input type="checkbox" name = "my_form[]" value= "Tree House">Tree House  <br/>
            <input type="checkbox" name = "my_form[]" value = "Fraternity Club">Fraternity Club<br/>
            <input type="checkbox" name = "my_form[]" value = "Utility Barack">Utility Barack <br/>
            <input type="checkbox" name = "my_form[]" value = "Oak Complex">Oak Complex<br/>
            <input type = "submit" name = "submit2" value = "submited2">


    </form>

    <?php
    if(isset($_POST['submit2'])){
        $buildings = (sizeof($_POST['my_form']));
        echo "<p>". "You selected ".$buildings." door";
        // put an s on plural
        if($buildings > 1) echo "(s): "; else echo ": ";
        //check the buildings selected by the user
        foreach($_POST['my_form'] as  $value){
            echo $value . " ";
        }
            echo "<br>";
            echo "Price is ".($buildings * 10) . "</p>";
//            echo "size " . count($_POST) . "<br>";
//            print_r($_POST);


    }
    ?>
</div>

<?php include "../footer.php"; ?>
</body>
</html>