<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/29/13
 * Time: 6:01 PM
 *
 */

require("./fpdf/fpdf.php");
?>

<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href= "../style.css">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<html>
<head>
    <title>A simple Web Shop </title>
</head>
<body>
<?php  include("../navigation.php"); ?>

<h1> A great but simple web-shop </h1>


<form name = "web_shop_data" method = "post" action="pdf.php">

    <div class="item_block">
        <!--type 1-->
        <label for = "item1"> Item 1
            <select name="item 1">
                <option>Beer</option>
                <option>Coca Cola</option>
                <option>Bottled water</option>
            </select>
        </label>

        <label for ="amount_item1"> Amount
            <input type="text" name="amount_item1">
        </label>

    </div>

    <div class="item_block">
        <!--type 2-->
        <label for="item2"> Item 2
            <select name="item 2">
                <option>Mars bar</option>
                <option>Twix bar</option>
                <option>Bounty bar</option>
            </select>
        </label>

        <label for="amount_item2"> Amount
            <input type="text" name="amount_item2">
        </label>
    </div>


    <h1>Customer Information</h1>

    <div id="user_data">
        <label for ="customer_name">Name <br/>
            <input type ="text" name="customer_name">
        </label>
        <label for="customer_address">Address<br/>
            <input type="text" name="customer_address">
        </label>
        <label for="customer_city">City <br/>
            <input type="text" name="customer_city">
        </label>

        <input type="submit" name="order"value="Order">

    </div>
    </form>


<?php
if(isset($_POST['order'])){
   /*print_r($_POST);*/
    //Array ( [item_1] => Beer [amount_item1] => 2 [item_2] => Mars bar [amount_item2] => 4
    //[customer_name] => oscar [customer_address] => felipe vej 3 [customer_city] => copenhagen [order] => Order )

}

?>



<?php include "../footer.php"; ?>
</body>

</html>
