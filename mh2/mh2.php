<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/29/13
 * Time: 12:07 AM
 * To change this template use File | Settings | File Templates.
 */


?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href= "../style.css">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
    <head>
        <title> PHP Web Development - Experiments 2 </title>
    </head>
<body>
<? include "../navigation.php" ?>
<div class="experiments">
<h1>Experiments 2</h1>
<ul>
    <li><a href="/mh2/mh2_1.php">list of cities</a></li>
    <li><a href="/mh2/mh2_2.php">arrays & HTML checkboxes</a></li>
    <li><a href="/mh2/login.php">login system</a></li>
    <li><a href="/mh2/login2.php">login system extended</a></li>
    <li><a href="/mh2/web-shop.php">web shop - pdf</a></li>
</ul>
</div>

</body>

<?php include "../footer.php" ?>
</html>