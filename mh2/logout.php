<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/21/13
 * Time: 6:18 PM
 * called when teh user click the link to logout
 */

include 'security.php';
secure_session_start();
// Unset all session values
$_SESSION = array();
// get session parameters
$params = session_get_cookie_params();
// Reset the actual cookie.
setcookie(session_name(), '', time() - 48000, $params["path"], $params["domain"], $params["secure"], $params["http_only"]);
// Destroy session
session_destroy();
header('Location: ./login.php');