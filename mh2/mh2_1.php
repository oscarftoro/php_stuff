<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 9/25/13
 * Time: 10:07 AM
 *
 */

$cities = array( "Tokyo", "Mexico City", "New York City", "Mumbai", "Seoul", "Shanghai", "Lagos", "Buenos Aires", "Cairo", "London");

?>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">

    <link href="../style.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php include "../navigation.php" ?>
<h1>Some Cities</h1>
<p>
<?php
for($i=0;$i < count($cities); $i++){
    if($i == (count($cities)-1))
       echo $cities[$i];
    else
        echo "{$cities[$i]}" . ", ";

}
echo "</p> ";

function present_array($array){

    foreach($array as $city){
        echo "<ul><li> $city</li></ul>";
    }

}

// sort array
sort($cities)
//

?>
<div class="note">
<h1> Cities sorted and presented in an Unordered List</h1>

<?php

 present_array($cities);

echo "<div id =\"note2\">";
?>
</div>
<div class="note2">
<h1>Add the following cities to our collection: Los Angeles, Calcutta, Osaka, Beijing</h1>

<?php
//add some more cities
$cities[] =("Los Angeles");
$cities[] =("Calcutta");
$cities[] =("Osaka");
$cities[] =("Beijing");

sort($cities); //sort the array

present_array($cities);

?>
</div>
<?php include "../footer.php"; ?>
</body>
</html>