<?php
/**
 * Created by IntelliJ IDEA.
 * User: oscar
 * Date: 10/2/13
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
/*display errors for debug purposes*/
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

require "security.php";

secure_session_start();
if(isset($_POST['submit']) ){

    $username = $_POST['user'];
    $password = $_POST['password'];

    $db = new mysqli("localhost","user", "password","db");
    $sql = " SELECT  * FROM users WHERE user= ? and password= ?";
    $statement = $db -> prepare($sql);

    $statement->bind_param("ss", $username, $password);

    $statement->execute();
    $statement->bind_result($id,$db_name,$db_password);

    //fetch values
    $statement->fetch();

    if(($username == $db_name) && ($password == $db_password)) {

        $_SESSION['user'] = $username;
        $_SESSION['opgave'] = "/mh2/logout.php";
        header("Location: " . "/mh2/enter.php");
        exit;

    } else {

        echo " User or password incorrect...";
        echo $db_password;
    }

}
?>
<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href= "../style.css" media="screen">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<html>
<head>
    <title> Image gallery - Experiments 1 </title>
</head>
<body>
<? include "../navigation.php" ?>


<div class ="login">
    <h1>Login </h1>
<form name="login" method ="post" action ="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" >
    <label for ="text">  User name
        <input type="text" name="user">
    </label> <br/>
    <label for ="password"> Password
        <input type="password" name="password">
    </label>  <br/>
 <input type ="submit" name="submit" value="login">  <br/>
</form>
</div>

<?php include "../footer.php"; ?>
</body>
</html>